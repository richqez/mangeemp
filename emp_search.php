<?php 
	
	include 'function.php';
	session_start();
	check_login();

 ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Search Employee</title>
		<?php include 'head.php'; ?>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<?php include "navbar.php" ?>
			</div>
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">

					 <div class="row">
					 	<div class="col-md-6 col-md-offset-3">
					 		<form action="emp_search.php" method="POST" role="form" class="form-inline">
					 			<legend>ค้นหาพนักงาน</legend>
					 		
					 			<div class="col-md-8 col-md-offset-2">
					 				<div class="form-group">
						 				<label>คำค้นหา</label>
						 				<input type="text" name="keyword" class="form-control" id="" placeholder="ชื่อพนักงงาน,รหัสพนักงาน">
						 			</div>
						 			<button type="submit" name="search" class="btn btn-primary">ค้นหา</button>
					 			</div>
					 		</form>
					 	</div>
					 </div>
					 <div class="row" style="padding-top:2%">
					 	<div class="col-md-12">
					 		<table class="table table-hover" id="emp_table">
					 			<thead>
					 				<tr>
					 					<th>รหัสพนักงาน</th>
					 					<th>ตำแหน่ง</th>
					 					<th>ชื่อ</th>
					 					<th>นามสกุล</th>
					 					<th>วันที่เริ่มทำงาน</th>
					 				</tr>
					 			</thead>
					 			<tbody>

					 				<?php if (isset($_POST['search'])): ?>
										<?php 
														
														
														$keyword = $_POST['keyword'];

														$sql_statement = "SELECT * FROM employee WHERE emp_firstname LIKE '%$keyword' OR emp_id LIKE '%$keyword%'";

														$db = connect_db();

														$result = $db->query($sql_statement);

														if (!$result) {
															    throw new Exception("Database Error [{$this->database->errno}] {$this->database->error}");
														}
										 ?>
										
										<?php if (!$result->num_rows < 1 ): ?>
											<?php while ($row = $result->fetch_assoc()) : ?>
												<tr>
													<td><?php echo $row['emp_id']; ?></td>
													<td><?php echo $row['emp_role'] ?></td>
													<td><?php echo $row['emp_firstname']; ?></td>
													<td><?php echo $row['emp_lastname']; ?></td>
													<td><?php echo $row['emp_startworkdate']; ?></td>
													<td><a class="btn btn-success" href="emp_update.php?update=<?php echo $row['emp_id']; ?>" role="button">อัพเดรต</a></td>
													<td><a class="btn btn-danger" delete-link href="emp_delete.php?delete=<?php echo $row['emp_id'] ?>" role="button">ลบข้อมูล</a></td>
												</tr>

											<?php endwhile; ?>
											<?php else: ?>
												<tr>
													<td style="text-align:center" colspan="4"><b>ไม่พบผลลัพะ์ที่ตรงกับ </b>" <?php echo $_POST['keyword'] ; ?> "</td>
												</tr>
										<?php endif ?>


									<?php endif ?>

					 			</tbody>
					 		</table>
					 		<?php if (isset($_POST['search'])): ?>
					 			<b>ผลลัพธ์ทั้งหมด <?php echo $result->num_rows ?> รายการ</b>
					 		<?php endif ?>
					 	</div>
					 </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<script>
	
jQuery(document).ready(function($) {
	$('#emp_table').on('click', 'a[delete-link]', function(event) {
		var con = confirm("คุณต้องการลบข้อมูล ?");
		if (!con) {
			event.preventDefault();
		};
	});
});

</script>