<?php 
	include 'function.php';
	session_start();
	check_login();
	check_role("admin");
 ?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<?php include 'head.php'; ?>
	</head>
	<body>
		<div class="container">
			
			<div class="row">
				<?php include "navbar.php" ?>
			</div>

			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">

					 <div class="col-md-6 col-md-offset-1">
					 	<form action="emp_save.php" method="POST" class="form-horizontal" role="form">
					 			<div class="form-group">
					 				<legend>เพิ่มพนักงาน</legend>
					 			</div>
					 			
					 			<div class="col-md-6">

					 				<div class="form-group">
						 				<label>Username</label>
						 				<input type="text" name="username" id="inputUsername" class="form-control" value="" required="required" >
						 			</div>

						 			<div class="form-group">
						 				<label>Username</label>
						 				<input type="password" name="password" id="inputPassword" class="form-control" value="" required="required" >
						 			</div>


					 				<div class="form-group">
						 				<label>ชื่อ</label>
						 				<input type="text" name="firstname" id="inputFirstname" class="form-control" value="" required="required" >
						 			</div>
						 			
						 			<div class="form-group">
						 				<label>นามสกุล</label>
						 				<input type="text" name="lastname" id="inputLastname" class="form-control" value="" required="required" >
						 			</div>

						 			<div class="form-group">
						 				<label>เพศ</label>
						 				<div class="radio">
						 					<label>
						 						<input type="radio" name="sex"  value="m" checked="checked">
						 						Male
						 					</label>
						 				</div>
						 				<div class="radio">
						 					<label>
						 						<input type="radio" name="sex"  value="f" checked="checked">
						 						Female
						 					</label>
						 				</div>
						 			</div>

						 			<div class="form-group">
						 				<label>วันเกิด</label>
						 				<input type="date" name="birthdate" id="inputBirthdate" class="form-control" value="" >
						 			</div>

						 			<div class="form-group">
						 				<label>ที่อยู่</label>
						 				<textarea name="address" id="inputAddress" class="form-control" rows="3" required="required"></textarea>
						 			</div>

						 			<div class="form-group">
						 				<label>วันที่เข้าทำงาน</label>
						 				<input type="date" name="startworkdate" id="inputStartwork" class="form-control" value="" >
						 			</div>

						 			<div class="form-group">
						 				<label>ตำแหน่ง</label>
						 				<select name="role" id="inputRole" class="form-control" required="required">
						 					<option value="admin">admin</option>
						 					<option value="user">user</option>
						 				</select>
						 			</div>

						 			<div class="form-group">
						 				<button type="submit" name="add_emp" class="btn btn-primary">Submit</button>
						 				<input type="reset" value="Reset" class="btn btn-primary">
						 			</div>
					 			</div>
					 	</form>
					 </div>

					</div>
				</div>
			</div>
		</div>
	</body>
</html>