-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 26, 2015 at 08:44 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mange_emp`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `emp_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `emp_firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `emp_lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `emp_sex` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `emp_birthdate` date NOT NULL,
  `emp_address` text COLLATE utf8_unicode_ci NOT NULL,
  `emp_startworkdate` date NOT NULL,
  `emp_role` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_username`, `emp_password`, `emp_firstname`, `emp_lastname`, `emp_sex`, `emp_birthdate`, `emp_address`, `emp_startworkdate`, `emp_role`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 'f', '2015-10-01', '                                            admin                                       ', '2015-10-01', 'admin'),
(3, 'WHERExxxxxxx', 'WHERE', 'WHEREWHERE', 'WHERE', 'f', '2015-09-29', '                                                                                        WHERE                                                                               ', '2015-10-15', ''),
(4, 'localho', 'localho', 'localho', 'localho', 'f', '2015-10-02', 'localho', '2015-10-05', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
