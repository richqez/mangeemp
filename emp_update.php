<?php 
	
	include 'function.php';

	if (isset($_GET['update'])) {
		
		$emp_id = $_GET['update'];

		$db = connect_db();

		$sql_statement = "SELECT * FROM employee WHERE emp_id=$emp_id";

		$result = $db->query($sql_statement);

		$row = $result->fetch_assoc();
	}

 ?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<?php include 'head.php'; ?>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">

					 <div class="col-md-6 col-md-offset-1">
					 	<form action="emp_save.php" method="POST" class="form-horizontal" role="form">
					 			<div class="form-group">
					 				<legend>แก้ไขข้อมูลพนักงาน</legend>
					 			</div>
					 			
					 			<div class="col-md-6">

					 				<div class="form-group">
						 				<label>Username</label>
						 				<input type="text" name="username" id="inputUsername" class="form-control" value="<?php echo $row['emp_username'] ?>" required="required" >
						 			</div>

						 			<div class="form-group">
						 				<label>Username</label>
						 				<input type="password" name="password" id="inputPassword" class="form-control" value="<?php echo $row['emp_password'] ?>" required="required" >
						 			</div>


					 				<div class="form-group">
						 				<label>ชื่อ</label>
						 				<input type="text" name="firstname" id="inputFirstname" class="form-control" value="<?php echo $row['emp_firstname'] ?>" required="required" >
						 			</div>
						 			
						 			<div class="form-group">
						 				<label>นามสกุล</label>
						 				<input type="text" name="lastname" id="inputLastname" class="form-control" value="<?php echo $row['emp_lastname'] ?>" required="required" >
						 			</div>

						 			<div class="form-group">
						 				<label>เพศ</label>
						 				<div class="radio">
						 					<label>
						 						<input type="radio" name="sex"  value="m" <?php if ($row['emp_sex'] == 'm') {echo 'checked="checked"';} ?>  >
						 						Male
						 					</label>
						 				</div>
						 				<div class="radio">
						 					<label>
						 						<input type="radio" name="sex"  value="f" <?php if ($row['emp_sex'] == 'f') {echo 'checked="checked"';} ?> >
						 						Female
						 					</label>
						 				</div>
						 			</div>

						 			<div class="form-group">
						 				<label>วันเกิด</label>
						 				<input type="date" name="birthdate" id="inputBirthdate" class="form-control" value="<?php echo $row['emp_birthdate'] ?>" >
						 			</div>

						 			<div class="form-group">
						 				<label>ที่อยู่</label>
						 				<textarea name="address" id="inputAddress" class="form-control" rows="3" required="required">
						 					<?php echo $row['emp_address'] ?>
						 				</textarea>
						 			</div>

						 			<div class="form-group">
						 				<label>วันที่เข้าทำงาน</label>
						 				<input type="date" name="startworkdate" id="inputStartwork" class="form-control" value="<?php echo $row['emp_startworkdate'] ?>" >
						 			</div>
									
									<input type="hidden" name="emp_id" value="<?php echo $row['emp_id'] ?>">

									<div class="form-group">
						 				<label>ตำแหน่ง</label>
						 				<select name="role" id="inputRole" class="form-control" required="required">
						 					<option value="admin"  <?php if ($row['emp_role'] == 'admin') { echo "selected"; } ?> >admin</option>
						 					<option value="user"  <?php if ($row['emp_role'] == 'user') { echo "selected"; } ?> >user</option>
						 				</select>
						 			</div>

						 			<div class="form-group">
						 				<button type="submit" name="update_emp" class="btn btn-primary">Submit</button>
						 				<a class="btn btn-danger" href="emp_search.php" role="button">ยกเลิก</a>
						 			</div>
					 			</div>
					 	</form>
					 </div>

					</div>
				</div>
			</div>
		</div>
	</body>
</html>