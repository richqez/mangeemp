
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<?php include "head.php";?>
		<style>
			body{
				  background: #00589F;
				  color: white;
				  background: -webkit-linear-gradient(90deg, #556270 10%, #FF6B6B 90%); /* Chrome 10+, Saf5.1+ */
				  background:    -moz-linear-gradient(90deg, #556270 10%, #FF6B6B 90%); /* FF3.6+ */
				  background:     -ms-linear-gradient(90deg, #556270 10%, #FF6B6B 90%); /* IE10 */
				  background:      -o-linear-gradient(90deg, #556270 10%, #FF6B6B 90%); /* Opera 11.10+ */
				  background:         linear-gradient(90deg, #556270 10%, #FF6B6B 90%); /* W3C */
				        
				}

				div.well{
				  height: 250px;
				} 

				.Absolute-Center {
				  margin: auto;
				  position: absolute;
				  top: 0; left: 0; bottom: 0; right: 0;

				}

				.Absolute-Center.is-Responsive {
				  width: 50%; 
				  height: 50%;
				  min-width: 200px;
				  max-width: 400px;
				  padding: 40px;
				}


		</style>
	</head>
	<body>
			
	<div class="container">
	  <div class="row">
	    <div class="Absolute-Center is-Responsive">
		



	      <div class="col-sm-12 col-md-10 col-md-offset-1" style="border: 1px solid;
    padding: 35px;
    border-radius: 10px;">

			<?php if (isset($_POST['login'])): ?>
		
			<?php 
		
					include "function.php";
					$db = connect_db();

					$username = $_POST['username'];
					$password =$_POST['password'];

					$sql_statement = "SELECT * FROM employee WHERE emp_username='$username' AND emp_password='$password'";
					

					$result = $db->query($sql_statement);


					if (!$result) {
						throw new Exception("Database Error [{$this->database->errno}] {$this->database->error}");
					}
					
			 ?>

			 <?php if ($result->num_rows < 1 ): ?>
			 	
			 	<?php echo "ชื่อผู้ใช้หรือรหัสผ่านผิด" ?>
			
			<?php else: ?>
				<?php  

				 session_start();
				 $_SESSION["userLogin"] = $result->fetch_assoc();
				 session_write_close();
				 header("location: emp_search.php");
					


				 ?>
			 <?php endif ?>

		 <?php endif ?>

	        <form action="login.php" id="loginForm" method="post">
	          <div class="form-group input-group">
	            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	            <input class="form-control" type="text" name='username' placeholder="username"/>          
	          </div>
	          <div class="form-group input-group">
	            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
	            <input class="form-control" type="password" name='password' placeholder="password"/>     
	          </div>
	          <div class="form-group">
	            <button type="submit" name="login" class="btn btn-primary btn-block">Login</button>
	          </div>
	        </form>        
	      </div>  
	    </div>    
	  </div>
	</div>
	</body>
</html>