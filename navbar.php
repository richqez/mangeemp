<div class="row">
	<nav class="navbar navbar-default" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav navbar-right">
			<li><a href="emp_search.php"><i class="glyphicon glyphicon-search"></i> ค้นหาพนักงาน / ลบ แก้ไข</a></li>
			<li><a href="emp_add.php"><i class="glyphicon glyphicon-plus"></i> เพิ่มพนักงาน</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th-list"></i> บัญชี<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="logout.php">ออกจากระบบ</a></li>
				</ul>
			</li>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav>
</div>